Rails.application.routes.draw do
	root :to => "invoice_bills#index"
  resources :collections
  resources :invoice_bills
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
