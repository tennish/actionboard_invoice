require 'test_helper'

class InvoiceBillsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @invoice_bill = invoice_bills(:one)
  end

  test "should get index" do
    get invoice_bills_url
    assert_response :success
  end

  test "should get new" do
    get new_invoice_bill_url
    assert_response :success
  end

  test "should create invoice_bill" do
    assert_difference('InvoiceBill.count') do
      post invoice_bills_url, params: { invoice_bill: { amount: @invoice_bill.amount, balance_due: @invoice_bill.balance_due, brand_manager: @invoice_bill.brand_manager, customer: @invoice_bill.customer, date: @invoice_bill.date, narration: @invoice_bill.narration, reference_id: @invoice_bill.reference_id, total_collection: @invoice_bill.total_collection } }
    end

    assert_redirected_to invoice_bill_url(InvoiceBill.last)
  end

  test "should show invoice_bill" do
    get invoice_bill_url(@invoice_bill)
    assert_response :success
  end

  test "should get edit" do
    get edit_invoice_bill_url(@invoice_bill)
    assert_response :success
  end

  test "should update invoice_bill" do
    patch invoice_bill_url(@invoice_bill), params: { invoice_bill: { amount: @invoice_bill.amount, balance_due: @invoice_bill.balance_due, brand_manager: @invoice_bill.brand_manager, customer: @invoice_bill.customer, date: @invoice_bill.date, narration: @invoice_bill.narration, reference_id: @invoice_bill.reference_id, total_collection: @invoice_bill.total_collection } }
    assert_redirected_to invoice_bill_url(@invoice_bill)
  end

  test "should destroy invoice_bill" do
    assert_difference('InvoiceBill.count', -1) do
      delete invoice_bill_url(@invoice_bill)
    end

    assert_redirected_to invoice_bills_url
  end
end
