class InvoiceBillsController < ApplicationController
  before_action :set_invoice_bill, only: [:show, :edit, :update, :destroy]
  #extend CodeInvoice

  # GET /invoice_bills
  # GET /invoice_bills.json
  def index
    if params[:status].present?
      @invoice_bills = InvoiceBill.status(params[:status])
    else
      @invoice_bills = InvoiceBill.all
    end  
  end

  # GET /invoice_bills/1
  # GET /invoice_bills/1.json
  def show
    @collections = @invoice_bill.reference.collections
  end

  # GET /invoice_bills/new
  def new
    @invoice_bill = InvoiceBill.new
  end

  # GET /invoice_bills/1/edit
  def edit
  end

  # POST /invoice_bills
  # POST /invoice_bills.json
  def create
    @invoice_bill = InvoiceBill.new(invoice_bill_params.merge(reference_id: Reference.create(name: params[:invoice_bill][:reference_id]).id))
    respond_to do |format|
      if @invoice_bill.save
        format.html { redirect_to @invoice_bill, notice: 'Invoice bill was successfully created.' }
        format.json { render :show, status: :created, location: @invoice_bill }
      else
        format.html { render :new }
        format.json { render json: @invoice_bill.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /invoice_bills/1
  # PATCH/PUT /invoice_bills/1.json
  def update
    respond_to do |format|
      if @invoice_bill.update(invoice_bill_params)
        format.html { redirect_to @invoice_bill, notice: 'Invoice bill was successfully updated.' }
        format.json { render :show, status: :ok, location: @invoice_bill }
      else
        format.html { render :edit }
        format.json { render json: @invoice_bill.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /invoice_bills/1
  # DELETE /invoice_bills/1.json
  def destroy
    @invoice_bill.destroy
    respond_to do |format|
      format.html { redirect_to invoice_bills_url, notice: 'Invoice bill was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_invoice_bill
      @invoice_bill = InvoiceBill.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def invoice_bill_params
      params.require(:invoice_bill).permit( :date, :customer, :brand_manager, :narration, :amount, :total_collection, :balance_due)
    end
end
