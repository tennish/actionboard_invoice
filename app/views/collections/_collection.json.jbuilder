json.extract! collection, :id, :amount, :date, :reference_id, :created_at, :updated_at
json.url collection_url(collection, format: :json)
