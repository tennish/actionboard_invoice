json.extract! invoice_bill, :id, :reference_id, :date, :customer, :brand_manager, :narration, :amount, :total_collection, :balance_due, :created_at, :updated_at
json.url invoice_bill_url(invoice_bill, format: :json)
