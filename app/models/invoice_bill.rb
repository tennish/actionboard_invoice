class InvoiceBill < ApplicationRecord
  belongs_to :reference
  has_many :collections, through: :reference

  def self.status(status)
  	case status
  	  when "pending"
         InvoiceBill.includes(:collections).where(collections: { id: nil })
      when "complete"
         InvoiceBill.joins(:collections)
    end   	
  		
  end	
end
