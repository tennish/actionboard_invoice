module InvoiceBillsHelper
	def total_collections(reference_id)
    Reference.find(reference_id).collections.pluck(:amount).sum
  end	

  def balance_due(total_amount,reference_id)
    total_amount + Reference.find(reference_id).collections.pluck(:amount).sum
  end	
end
