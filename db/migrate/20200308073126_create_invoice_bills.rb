class CreateInvoiceBills < ActiveRecord::Migration[5.1]
  def change
    create_table :invoice_bills do |t|
      t.belongs_to :reference, foreign_key: true
      t.string :name
      t.date :date
      t.string :customer
      t.string :brand_manager
      t.string :narration
      t.integer :amount
      t.string :total_collection
      t.string :balance_due

      t.timestamps
    end
  end
end
