class CreateCollections < ActiveRecord::Migration[5.1]
  def change
    create_table :collections do |t|
      t.integer :amount
      t.date :date
      t.string :name
      t.belongs_to :reference, foreign_key: true

      t.timestamps
    end
  end
end
